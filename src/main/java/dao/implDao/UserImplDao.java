package dao.implDao;

import dao.UserDao;
import entity.User;
import org.hibernate.Session;
import org.hibernate.query.Query;
import utile.HibernateUtile;

import java.util.List;

public class UserImplDao implements UserDao {

    private static Session session;

    public User create(User user) {
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            session.save(user);
            session.getTransaction().commit();
        }catch (Exception e){
            e.printStackTrace();
            System.out.println("user didn't create");
        }finally {
            session.close();
        }
        return user;
    }

    public List<User> read(int id)  {
        List list = null;
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            Query query = session.createQuery("from User where id = :id");
            query.setLong("id",id);
            list = query.list();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return list;
    }

    public List<User> readAll() {
        List list = null;
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            Query query = session.createQuery("from User");
            list = query.list();
            session.getTransaction().commit();

        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }

        return list;
    }

    public User update(User user) {
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            session.update(user);
            session.getTransaction().commit();
            System.out.println("User updated!");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
        return user;
    }

    public void delete(User user) {
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            session.delete(user);
            session.getTransaction().commit();
            System.out.println("User deleted");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }

    public void delete(int id){
        try {
            session = HibernateUtile.getSessionFactory().openSession();
            session.getTransaction().begin();
            Query query = session.createQuery("delete from User where id = :id");
            query.setLong("id",id);
            session.delete(query);
            session.getTransaction().commit();
            System.out.println("User deleted");
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            session.close();
        }
    }
}
