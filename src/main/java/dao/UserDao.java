package dao;

import entity.User;

import java.util.List;

public interface UserDao {

    public User create(User user);

    public List<User> read(int id);

    public List<User> readAll();

    public User update(User user);

    public void delete(User user);
}
