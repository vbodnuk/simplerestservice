package utile;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public class HibernateUtile {
    private static SessionFactory sessionFactory;
    static{
        sessionFactory = new Configuration().configure().buildSessionFactory();
    }
    public static SessionFactory getSessionFactory(){
        return sessionFactory;
    }
    public static void shutdown(){
        sessionFactory.close();
    }
}
