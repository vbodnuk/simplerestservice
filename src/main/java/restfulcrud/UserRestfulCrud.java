package restfulcrud;

import dao.implDao.UserImplDao;
import entity.User;

import javax.ws.rs.*;
import java.util.List;


@Path("/user")
public class UserRestfulCrud {

    UserImplDao userImplDao = new UserImplDao();


    @Path("/all")
    @GET
    @Produces(value = {"text/xml"})
    public List<User> getUser(){
        List<User> users = userImplDao.readAll();
        return users;
    }

    @Path("/{id}")
    @GET
    @Produces(value = {"text/xml"})
    public List<User> getById(@PathParam("id") int id){
        List<User> users = userImplDao.read(id);
        return users;
    }

    @Path("/add")
    @POST
    @Consumes(value = {"text/xml"})
    public void addUser(User user){
         userImplDao.create(user);
    }

    @Path("/update")
    @PUT
    @Produces(value = {"text/xml"})
    public void updateUser(User user){
         userImplDao.update(user);
    }

    @Path("/remove")
    @DELETE
    @Produces(value = {"text/xml"})
    public void deleteUser(User user){
        userImplDao.delete(user);
    }

    @Path("/remove/{id}")
    @DELETE
    @Produces(value = {"text/xml"})
    public void deleteUser(@PathParam("id") int id){
        userImplDao.delete(id);
    }
}
